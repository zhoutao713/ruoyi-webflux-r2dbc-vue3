package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.biz.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.mapper.SysNoticeMapper;
import com.ruoyi.system.service.ISysNoticeService;

/**
 * 公告 服务层实现
 * 
 * @author ruoyi
 */
@Service
public class SysNoticeServiceImpl
	extends BaseServiceImpl<SysNotice, Long, SysNoticeMapper>
	implements ISysNoticeService
{

}
