package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.biz.IService;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.system.domain.SysOperLog;

/**
 * 操作日志 服务层
 * 
 * @author ruoyi
 */
public interface ISysOperLogService extends IService<SysOperLog, Long>
{
    /**
     * 清空操作日志
     */
    public void cleanOperLog();
}
