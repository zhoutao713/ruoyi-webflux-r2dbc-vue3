package com.ruoyi.quartz.service;

import com.ruoyi.common.biz.IService;
import com.ruoyi.quartz.domain.SysJobLog;
import reactor.core.publisher.Mono;

/**
 * 定时任务调度日志信息信息 服务层
 *
 * @author ruoyi
 */
public interface ISysJobLogService extends IService<SysJobLog, Long> {
	/**
	 * 清空任务日志
	 *
	 * @return
	 */
	public Mono<Long> cleanJobLog();
}
